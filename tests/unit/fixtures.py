import functools
import json
import os

local_file = functools.partial(os.path.join, os.path.dirname(__file__))
TEST_EVENT = json.load(open(local_file('test_event.json')))
TEST_CONSOLE_HTML = open(local_file('test_console.html')).read()
