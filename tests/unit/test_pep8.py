from unittest import mock

import testtools

from . import fixtures
from pgb import pep8


def get_log_mock():
    return mock.Mock(spec_set=['debug', 'info', 'warning', 'error', 'critical',
                               'exception'])


class Pep8Test(testtools.TestCase):
    def test_get_console_log_url(self):
        url = pep8.get_console_log_url(get_log_mock(), fixtures.TEST_EVENT)
        expected_url = 'http://logs.openstack.org/14/102214/1/check/' \
                       'gate-stackalytics-pep8/d7080f8/console.html'
        self.assertEqual(expected_url, url)

    def test_get_revision_url(self):
        url = pep8.get_revision_url(fixtures.TEST_EVENT)
        expected_url = '/changes/stackforge%2Fstackalytics~master~' \
                       'I633092a9541b50032543962c9551de8ff23989db/revisions/' \
                       '4468b0b4a049f55a2d738fc1d78ef3673995cf73/'
        self.assertEqual(expected_url, url)

    def test_get_review(self):
        review = pep8.get_review(get_log_mock(), fixtures.TEST_CONSOLE_HTML,
                                 ['stackalytics/dashboard/web.py'])
        self.assertTrue(review)
        expected = {'stackalytics/dashboard/web.py': [
            {'line': '31', 'message': "F821 undefined name 'breaking_pep8'"},
        ]}
        self.assertEqual(expected, review["comments"])
