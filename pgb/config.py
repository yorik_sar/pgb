class MissingConfigVariable(Exception):
    pass


class Config:
    _missing = object()

    def __init__(self, config_dict):
        self._original = config_dict
        self.debug = self._get('debug', default=False)
        self.user = self._get('user')
        self.host = self._get('host')
        self.ssh_port = self._get('ssh_port', 29418)
        self.ssh_key = self._get('ssh_key', '')
        self.http_password = self._get('http_password')
        self.filters = self._get('filters', [])

    def _get(self, name, default=_missing):
        res = self._original.get(name, default)
        if res is self._missing:
            raise MissingConfigVariable(name)
        return res
