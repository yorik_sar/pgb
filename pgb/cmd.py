import argparse
import logging

import asyncio
import yaml

from . import config
from . import process

__all__ = ['main']


def _get_parser(prog=None):
    parser = argparse.ArgumentParser(
        prog=prog,
        description="Process stream of Gerrit events and convert all PEP8"
                    " fails to inline comments",
    )
    parser.add_argument(
        '-c', '--config',
        default="/etc/pgb.yaml",
        help="Path to config file.",
    )
    return parser


def _get_config_dict(filename):
    with open(filename, 'rb') as f:
        return yaml.safe_load(f)


def _setup_logging(config):
    if config.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(level=level)
    if config.debug:
        logging.getLogger('asyncio').setLevel('INFO')


def main(argv=None):
    args = _get_parser().parse_args(argv)
    config_obj = config.Config(_get_config_dict(args.config))
    _setup_logging(config_obj)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(process.main(config_obj))
    finally:
        loop.close()
