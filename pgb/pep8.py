import logging
import re
from urllib import parse as urlparse

import asyncio

from . import util

PEP8_RE = r'''(?xm)            # verbose, multiline
              ([a-zA-Z/.-_]+)  # filename
              :(\d+)           # line number
              :(\d+)           # column number
              :\ ([A-Z]\d\d\d) # error id
              \ (.*)$          # message
'''


@asyncio.coroutine
def process_event(gerrit, event):
    log = logging.getLogger(__name__ + '.process')
    log.debug('Processing event...')
    console_log_url = get_console_log_url(log, event)
    if not console_log_url:
        return
    revision_url = get_revision_url(event)
    console_log, changed_files = yield from asyncio.gather(
        util.get_url(console_log_url),
        gerrit.http('GET', revision_url + 'files/'),
    )
    review = get_review(log, console_log.decode('utf-8'), changed_files)
    if review:
        yield from gerrit.http('POST', revision_url + 'review', data=review)
    else:
        log.debug("No comments found. Nothing to post.")


def get_console_log_url(log, event):
    match = re.search('-pep8 (http[^\s]+) : ([A-Z]+)', event['comment'])
    if not match:
        log.debug("Can't find PEP8 results")
        return
    url, result = match.groups()
    if result == 'SUCCESS':
        log.debug("Nothing to do with PEP8-compilant changes")
        return
    return url + '/console.html'


def get_revision_url(event):
    return '/changes/%s~%s~%s/revisions/%s/' % tuple(map(urlparse.quote_plus, (
        event["change"]["project"],
        event["change"]["branch"],
        event["change"]["id"],
        event["patchSet"]["revision"],
    )))


def get_review(log, console_log, changed_files):
    review = {
        "message": "I noticed smth wrong with your PEP8 results.",
        "comments": {},
    }
    for error in re.finditer(PEP8_RE, console_log):
        filename, line, column, error, message = error.groups()
        if filename.startswith('./'):
            filename = filename[2:]
        if filename not in changed_files:
            log.debug("Error in %s while it is not part of the patchset",
                      filename)
            continue
        review["comments"].setdefault(filename, []).append({
            "line": line,
            "message": error + " " + message,
        })
    if review["comments"]:
        return review
    else:
        return
