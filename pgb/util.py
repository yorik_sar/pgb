import json
import logging

import aiohttp
import asyncio
from asyncio import subprocess

from . import digest


@asyncio.coroutine
def get_url(url):
    resp = yield from aiohttp.request('GET', url)
    return (yield from resp.read_and_close())


def decode_from_content_type(data, content_type):
    charset = 'utf-8'
    if content_type and ';' in content_type:
        parts = content_type.split('; charset=')
        if len(parts) > 1:
            charset = parts[1]
    return data.decode(charset)


class Gerrit:
    def __init__(self, config):
        if config.ssh_key:
            key_args = ('-i', config.ssh_key)
        else:
            key_args = ()
        self.ssh_cmd = ('ssh',) + key_args + (
            "-p", str(config.ssh_port),
            "{0}@{1}".format(config.user, config.host),
        )
        self.auth = (config.user, config.http_password)
        self.http_prefix = 'https://%s/a' % (config.host,)
        self.log = logging.getLogger(__name__ + '.Gerrit')

    @asyncio.coroutine
    def ssh(self, *cmd, get_process=False):
        process = yield from asyncio.create_subprocess_exec(
            *(self.ssh_cmd + cmd),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE  # no comma because of a BUG!!!
        )
        if get_process:
            return process
        out, err = yield from process.communicate()
        return process.returncode, out, err

    @asyncio.coroutine
    def http(self, method, url, data=None):
        if data:
            data = json.dumps(data)
        self.log.debug("HTTP request: %s %s with data %s", method, url, data)
        response = yield from digest.request(method, self.http_prefix + url,
                                             auth=self.auth, data=data)
        result = yield from response.read_and_close()
        self.log.debug("Got response: %s", result)
        content_type = response.headers.get('content-type')
        result = decode_from_content_type(result, content_type)
        return json.loads(result[5:])
