import json
import logging

import asyncio

from . import pep8
from . import util


def log_event(log, event, match):
    try:
        project = event["change"]["project"]
    except KeyError:
        return
    if 'stackalytics' in project:
        log.debug("Got event:\n%s",
                  json.dumps(event, indent=4, sort_keys=True))
        if not match:
            log.debug("Event doesn't match filters")


@asyncio.coroutine
def main(config):
    log = logging.getLogger(__name__ + '.main')
    gerrit = util.Gerrit(config)
    process = yield from gerrit.ssh("gerrit", "stream-events",
                                    get_process=True)
    while True:
        line = yield from process.stdout.readline()
        if not line:
            stderr = yield from process.stderr.read()
            log.error("ssh process finished. stderr:\n%s", stderr)
            return
        event = json.loads(line[:-1].decode('utf-8'))
        match = match_filters(event, config.filters)
        log_event(log, event, match)
        if match:
            asyncio.async(pep8.process_event(gerrit, event))


def match_filters(event, filters):
    if isinstance(filters, dict):
        try:
            return all(match_filters(event[key], value)
                       for key, value in filters.items())
        except (KeyError, TypeError):
            return False
    elif isinstance(filters, list):
        return any(match_filters(event, filter_) for filter_ in filters)
    else:
        return event == filters
    assert False, "We shouldn't get here"
