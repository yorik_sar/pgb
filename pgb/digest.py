import codecs
import hashlib
import logging
import os
import urllib.parse

import aiohttp
import asyncio

LOG = logging.getLogger(__name__)


@asyncio.coroutine
def request(method, url, *, auth=None, headers=None, **kwargs):
    auth_fails = 0
    while True:
        LOG.debug('Sending request with headers: %s', headers)
        resp = yield from aiohttp.request(method, url, headers=headers,
                                          **kwargs)
        if resp.status != 401 or not isinstance(auth, (tuple, list)):
            break
        auth_fails += 1
        if auth_fails > 1:
            break
        auth_header = resp.headers.get('www-authenticate')
        if not auth_header:
            LOG.debug("www-authenticate header not found")
            break
        auth_type, param_str = auth_header.split(' ', 1)
        if auth_type != 'Digest':
            LOG.debug("Unsupported authentication type: %s", auth_type)
            break
        params = _split_params(param_str)
        auth_resp = _get_response(method, url, auth, params)
        if not auth_resp:
            break
        if headers is None:
            headers = {}
        headers['authorization'] = "Digest " + _join_params(auth_resp)

    return resp


def _split_params(param_str):
    parts = map(str.strip, param_str.split(','))
    res = {}
    for part in parts:
        k, v = part.split('=', 1)
        if v[0] == '"':
            v = v[1:-1]
        res[k] = v
    return res


def _join_params(params):
    return ", ".join('%s="%s"' % (k, v) for k, v in params.items())


def _md5(*args):
    args = (arg if isinstance(arg, bytes) else arg.encode() for arg in args)
    return hashlib.md5(b":".join(args)).hexdigest()


def _get_response(method, url, auth, params):
    username, password = auth
    realm = params.get('realm')
    nonce = params.get('nonce')
    if not realm or not nonce:
        LOG.debug('realm or nonce is missing in authentication parameters')
        return
    response = {'username': username, 'realm': realm, 'nonce': nonce}
    cnonce = codecs.encode(os.urandom(16), 'hex_codec').decode()
    algo = params.get('algorithm')
    if algo is None or algo == 'MD5':
        HA1 = _md5(username, params['realm'], password)
    elif algo == 'MD5-ness':
        response['algorithm'] = algo
        response['cnonce'] = cnonce
        HA1 = _md5(_md5(username, params['realm'], password), nonce, cnonce)
    qop = params.get('qop')
    url_path = urllib.parse.urlparse(url).path
    if qop:
        qop = qop.split(',')
    if not qop or 'auth' in qop:
        response['uri'] = url_path
        HA2 = _md5(method, url_path)
    elif 'auth-int' in qop:
        LOG.debug("auth-int qop is not supported yet")
        return  # maybe implement later
    else:
        LOG.debug("Unsupported qop: %s", qop)
        return
    if not qop:
        response['response'] = _md5(HA1, nonce, HA2)
    else:
        if 'auth' in qop:
            qop = 'auth'
        else:
            qop = 'auth-int'
        response['qop'] = qop
        response['cnonce'] = cnonce
        response['nc'] = count = '00000001'
        response['response'] = _md5(HA1, nonce, count, cnonce, qop, HA2)
    opaque = params.get('opaque')
    if opaque is not None:
        response['opaque'] = opaque
    LOG.debug('Finished building response for parameters:\n%s\nResult is:\n%s',
              params, response)
    return response
