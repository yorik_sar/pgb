=======================
 pgb - PEP8 Gerrit Bot
=======================

This bot is intented to parse logs from your CI system and add inline comments
with all PEP8 errors found in change requests.
